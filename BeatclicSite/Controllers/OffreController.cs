﻿using System.Collections.Generic;
using BeatclicSite.Models;
using Microsoft.AspNetCore.Mvc;

namespace BeatclicSite.Controllers
{
    public class OffreController : Controller
    {
        public IActionResult Index()
        {
            var galerie = new List<Offre>()
            {
                new Offre(titre:"Koba / RR 9.1", url:"static.booska-p.com/images/news/niska-et-koba-rr-9-1-20h-videoclip-649.jpg", prix: 20),
                new Offre(titre:"niska / batimemt", url:"media.gqmagazine.fr/photos/5d710f4a0113f500080af79e/master/pass/niska%20_%20KORIA%201%20.png", prix: 15),
                new Offre(titre:"SCH HAMZA / HS", url:"static.booska-p.com/images/news/niska-et-koba-rr-9-1-20h-videoclip-649.jpg", prix: 13),
            };
            for(var i=0; i<galerie.Count; i++)
            {
                galerie[i].AugmenterLesPrix();
            }
            foreach(var photo in galerie)
            {
                photo.AugmenterLesPrix();
            }
            return View(galerie);
        }
    }
}