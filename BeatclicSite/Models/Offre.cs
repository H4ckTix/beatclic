﻿using System;

namespace BeatclicSite.Models
{
    public class Offre
    {
        private string titre;
        private string url;
        private int prix;

        public Offre()
        {
            titre = url = String.Empty;
        }
        public Offre(string titre, string url, int prix)
        {
            this.titre = titre;
            this.url = url;
            if(this.prix < 0)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(prix), 
                    "Le prix ne peux etre inferieure a 0 euro"
                );
            }
            this.prix = prix;
        }
        public void AugmenterLesPrix()
        {
            prix ++;
        }
        public string GetTitre()
        {
            return titre;
        }
        public string GetTitre2() => titre;
        public string Titre
        {
            get
            {
                return titre;
            }
            set
            {
                if(value.Length<2)
                {
                    throw new ArgumentException("Titre trop court");
                }
                titre = value;
            }
        }

        public int Prix
        {
            get => prix;
        }

        public string UrlHttps
        {
            get { return $"https://{url}"; }
        }
    }
}